#ifndef RMSCALC_H
#define RMSCALC_H

#include <cmath>
#include <deque>
#include <memory>

class RMSCalc
{
public:
    RMSCalc( const size_t NmbrReadings );
    ~RMSCalc();

    /**
     * @brief calculateRMS
     * Method used to calculate the RMS Value from the newValue inserted.
     * This method is meant to be called periodically and, for now, it's
     * implementation stands on that assumption. The amount of readings that
     * will be used is defined in MAX_SIZE, set by the constructor.
     * @param newValue
     * @param period
     * @return
     */
    float calculateRMS( float newValue, float period );

private:
    /*--- Pointers. Allocate it on the heap (May be too big for stack). ---*/
    std::unique_ptr< std::deque< float > > m_readingFifo;
    std::unique_ptr< std::deque< float > > m_periodFifo;

    float m_sum;

    const size_t MAX_SIZE;

};

#endif // RMSCALC_H
