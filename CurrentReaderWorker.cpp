#include "CurrentReaderWorker.h"

CurrentReaderWorker::CurrentReaderWorker(const QString &thingSpeakAPIWriteKey, QObject *parent ) :
    QObject( parent )
  , m_httpRequest( )
  , m_timer( new QTimer( this ) )
  , m_getReqTimer( new QTimer( this ) )
  , m_startAcqTimer( new QTimer( this ) )
  , m_doHTTPReqTimer( new QTimer( this ) )
  , m_lastTs( 0 )
  , m_currentTs( QDateTime::currentMSecsSinceEpoch() )
  , m_voltage{ 0.0f, 0.0f, 0.0f, 0.0f }
  , m_current{ 0.0f, 0.0f, 0.0f, 0.0f }
  , m_temperature( 0.0f )
  , m_luminosity( 0.0f )
  , m_sendQueue( )
  , m_rmsCalc( /*new RMSCalc( 1000 )*/ )
  , m_highPassFilter( /*new FIRFilter<float>( 301 )*/ )
  , m_outputFile( new QFile( this ) )
  , THINGSPEAK_WRITEAPI_KEY( thingSpeakAPIWriteKey )
  , THINGSPEAK_BASE_URL( "https://api.thingspeak.com/update?key=%1"
                                                                "&field1=%2"        //Current
                                                                "&field2=%3"        //Current
                                                                "&field3=%4"        //Current
                                                                "&field4=%5"        //Current
                                                                "&field5=%6"        //Luminosity
                                                                "&field6=%7"        //Temperature
                                                                "&created_at=%8" )
{
    connect( &m_httpRequest, &QNetworkAccessManager::finished, this, &CurrentReaderWorker::httpRequestReply );
    connect( m_timer, &QTimer::timeout, this, &CurrentReaderWorker::doWork );
    connect( m_getReqTimer, &QTimer::timeout, this, &CurrentReaderWorker::createHttpGETRequest, Qt::DirectConnection );
    connect( m_startAcqTimer, &QTimer::timeout, this, &CurrentReaderWorker::startWork );
    connect( m_doHTTPReqTimer, &QTimer::timeout, this, &CurrentReaderWorker::doHttpGetRequest );

    m_rmsCalc.resize( 4 );
    m_rmsCalc[0].reset( new RMSCalc( 1000 ) );
    m_rmsCalc[1].reset( new RMSCalc( 1000 ) );
    m_rmsCalc[2].reset( new RMSCalc( 1000 ) );
    m_rmsCalc[3].reset( new RMSCalc( 1000 ) );

    m_highPassFilter.resize( 4 );
    m_highPassFilter[0].reset( new FIRFilter<float>( 301 ) );
    m_highPassFilter[1].reset( new FIRFilter<float>( 301 ) );
    m_highPassFilter[2].reset( new FIRFilter<float>( 301 ) );
    m_highPassFilter[3].reset( new FIRFilter<float>( 301 ) );

    /*--- setup the spi in the raspberry PI ---*/
    wiringPiSetup();
    mcp3004Setup( BASE, SPI_CHAN );


    /*--- Start the fir filter. ---*/
    startHighPassFilter();

    /*--- Setup the output file. ---*/
    //m_outputFile.setFileName( "current-" + QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss")  + ".csv" );

    //m_outputFile.open( QFile::ReadWrite );

    //m_outputFileLine = QString( "%1, %2\n");
    //m_outputFile.write( m_outputFileLine.arg("ts").arg("current").toUtf8() );

    /*--- start the initial timer. ---*/
    m_startAcqTimer->setSingleShot( true );
    m_startAcqTimer->start( 15000 );

    /*--- Setup the timers. ---*/
    m_timer->start( RMS_CALC_TIMEOUT );
    m_getReqTimer->start( HTTP_UPDATE_TIMETOUT );
    m_doHTTPReqTimer->start( HTTP_DO_GET_TIMETOUT );
}

CurrentReaderWorker::~CurrentReaderWorker()
{
    //m_outputFile.flush();
    //m_outputFile.close();
}

void CurrentReaderWorker::startWork()
{
    qDebug() << Q_FUNC_INFO;
}

void CurrentReaderWorker::doWork()
{
    m_currentTs = QDateTime::currentMSecsSinceEpoch();

    m_voltage[0] = analogRead( BASE+0 )* AD_REF_VOLTAGE / static_cast< float > ( AD_COUNTINGS );
    m_voltage[1] = analogRead( BASE+1 )* AD_REF_VOLTAGE / static_cast< float > ( AD_COUNTINGS );
    m_voltage[2] = analogRead( BASE+2 )* AD_REF_VOLTAGE / static_cast< float > ( AD_COUNTINGS );
    m_voltage[3] = analogRead( BASE+3 )* AD_REF_VOLTAGE / static_cast< float > ( AD_COUNTINGS );

    m_temperature = ( analogRead( BASE+ 4 ) * AD_REF_VOLTAGE / static_cast< float > ( AD_COUNTINGS ) )*100 - 6;
    m_luminosity = ( 5 - analogRead( BASE+ 5 ) * AD_REF_VOLTAGE / static_cast< float > ( AD_COUNTINGS ) );

    float acVoltage[4];
    acVoltage[0] = m_highPassFilter[0]->addElementAndProcess( m_voltage[0] );
    acVoltage[1] = m_highPassFilter[1]->addElementAndProcess( m_voltage[1] );
    acVoltage[2] = m_highPassFilter[2]->addElementAndProcess( m_voltage[2] );
    acVoltage[3] = m_highPassFilter[3]->addElementAndProcess( m_voltage[3] );

    m_current[0] = m_rmsCalc[0]->calculateRMS( acVoltage[0], (m_currentTs - m_lastTs)/1000.0f )/(0.0075f);
    m_current[1] = m_rmsCalc[1]->calculateRMS( acVoltage[1], (m_currentTs - m_lastTs)/1000.0f )/(0.0075f);
    m_current[2] = m_rmsCalc[2]->calculateRMS( acVoltage[2], (m_currentTs - m_lastTs)/1000.0f )/(0.0075f);
    m_current[3] = m_rmsCalc[3]->calculateRMS( acVoltage[3], (m_currentTs - m_lastTs)/1000.0f )/(0.0075f);

    m_lastTs = m_currentTs;

    if( ! m_startAcqTimer->isActive() ) {
        //m_outputFile.write( m_outputFileLine.arg( m_currentTs ).arg( acVoltage ).toUtf8() );
        //qDebug() << m_current;
    }
}

void CurrentReaderWorker::httpRequestReply(QNetworkReply *reply)
{   
    quint64 replyTs = 0x00;
    foreach( quint64 ts,m_sendQueue.keys() ) {
        if( reply == m_sendQueue.value(ts).lastReplyObjct ) {
            replyTs = ts;
            break;
        }
    }

    if( replyTs == 0 ) return;

    if( reply->error() == QNetworkReply::NoError ) {
        qCritical() << Q_FUNC_INFO << "Removing correct response: " << replyTs;
        m_sendQueue.remove( replyTs );                              //Correctly sent.
    }else {
        qCritical() << Q_FUNC_INFO << "Error sendind: " << replyTs;
        m_sendQueue[ replyTs ].lastReplyObjct = Q_NULLPTR;    //Not sent. just set the pointer to null the get ready for another transmission.
    }

    reply->deleteLater();
}

void CurrentReaderWorker::createHttpGETRequest()
{
    if( ! m_startAcqTimer->isActive() ) {
        ToSendData toSendData;

        toSendData.ts = QDateTime::currentMSecsSinceEpoch();

        toSendData.current[0] = m_current[0];
        toSendData.current[1] = m_current[1];
        toSendData.current[2] = m_current[2];
        toSendData.current[3] = m_current[3];

        toSendData.luminosity = m_luminosity;
        toSendData.temperature = m_temperature;
        toSendData.lastReplyObjct = Q_NULLPTR;

        m_sendQueue.insert( toSendData.ts, toSendData );
    }
}

void CurrentReaderWorker::doHttpGetRequest()
{
    qCritical() << Q_FUNC_INFO << "Send queue size: " << m_sendQueue.size();

    if( m_sendQueue.keys().size() != 0 ) {
        if( m_sendQueue.size() > 0 ) {
            quint64 ts = m_sendQueue.first().ts;
            QUrl url( createHTTPGetURLFromDataStruct( m_sendQueue.value(ts) ) );
            qCritical() << Q_FUNC_INFO << "Sending: " << ts << " url: " << url;
            QNetworkReply *reply = m_httpRequest.get( QNetworkRequest( url ) );
            m_sendQueue[ ts ].lastReplyObjct = reply;
        }
    }
}

void CurrentReaderWorker::startHighPassFilter()
{
    std::vector< float > coef( { -0.000000,-0.000009,-0.000020,-0.000032,-0.000045,-0.000058,-0.000071,-0.000082,
                                 -0.000092,-0.000099,-0.000103,-0.000102,-0.000096,-0.000086,-0.000069,-0.000048,
                                 -0.000021,0.000011,0.000046,0.000085,0.000124,0.000163,0.000200,0.000233,0.000261,
                                 0.000281,0.000291,0.000290,0.000278,0.000253,0.000214,0.000163,0.000100,0.000027,
                                 -0.000056,-0.000144,-0.000234,-0.000325,-0.000411,-0.000489,-0.000554,-0.000604,
                                 -0.000634,-0.000642,-0.000626,-0.000584,-0.000515,-0.000420,-0.000300,-0.000159,
                                 0.000000,0.000172,0.000351,0.000531,0.000705,0.000866,0.001005,0.001116,0.001193,
                                 0.001230,0.001223,0.001168,0.001064,0.000912,0.000714,0.000474,0.000199,-0.000103,
                                 -0.000423,-0.000749,-0.001070,-0.001372,-0.001643,-0.001870,-0.002042,-0.002148,
                                 -0.002180,-0.002132,-0.001999,-0.001783,-0.001484,-0.001110,-0.000669,-0.000174,
                                 0.000360,0.000915,0.001472,0.002010,0.002508,0.002943,0.003296,0.003548,0.003683,
                                 0.003689,0.003556,0.003281,0.002865,0.002315,0.001642,0.000863,0.000000,-0.000920,
                                 -0.001866,-0.002806,-0.003703,-0.004522,-0.005227,-0.005785,-0.006165,-0.006339,
                                 -0.006289,-0.005998,-0.005462,-0.004682,-0.003668,-0.002440,-0.001027,0.000534,
                                 0.002199,0.003915,0.005624,0.007265,0.008772,0.010082,0.011129,0.011852,0.012196,
                                 0.012111,0.011558,0.010506,0.008937,0.006845,0.004238,0.001138,-0.002422,-0.006391,
                                 -0.010711,-0.015310,-0.020106,-0.025010,-0.029930,-0.034766,-0.039422,-0.043799,
                                 -0.047806,-0.051357,-0.054375,-0.056795,-0.058562,-0.059639,0.940000,-0.059639,
                                 -0.058562,-0.056795,-0.054375,-0.051357,-0.047806,-0.043799,-0.039422,-0.034766,
                                 -0.029930,-0.025010,-0.020106,-0.015310,-0.010711,-0.006391,-0.002422,0.001138,
                                 0.004238,0.006845,0.008937,0.010506,0.011558,0.012111,0.012196,0.011852,0.011129,
                                 0.010082,0.008772,0.007265,0.005624,0.003915,0.002199,0.000534,-0.001027,-0.002440,
                                 -0.003668,-0.004682,-0.005462,-0.005998,-0.006289,-0.006339,-0.006165,-0.005785,
                                 -0.005227,-0.004522,-0.003703,-0.002806,-0.001866,-0.000920,0.000000,0.000863,
                                 0.001642,0.002315,0.002865,0.003281,0.003556,0.003689,0.003683,0.003548,0.003296,
                                 0.002943,0.002508,0.002010,0.001472,0.000915,0.000360,-0.000174,-0.000669,-0.001110,
                                 -0.001484,-0.001783,-0.001999,-0.002132,-0.002180,-0.002148,-0.002042,-0.001870,
                                 -0.001643,-0.001372,-0.001070,-0.000749,-0.000423,-0.000103,0.000199,0.000474,
                                 0.000714,0.000912,0.001064,0.001168,0.001223,0.001230,0.001193,0.001116,0.001005,
                                 0.000866,0.000705,0.000531,0.000351,0.000172,0.000000,-0.000159,-0.000300,-0.000420,
                                 -0.000515,-0.000584,-0.000626,-0.000642,-0.000634,-0.000604,-0.000554,-0.000489,
                                 -0.000411,-0.000325,-0.000234,-0.000144,-0.000056,0.000027,0.000100,0.000163,
                                 0.000214,0.000253,0.000278,0.000290,0.000291,0.000281,0.000261,0.000233,0.000200,
                                 0.000163,0.000124,0.000085,0.000046,0.000011,-0.000021,-0.000048,-0.000069,
                                 -0.000086,-0.000096,-0.000102,-0.000103,-0.000099,-0.000092,-0.000082,-0.000071,
                                 -0.000058,-0.000045,-0.000032,-0.000020,-0.000009,-0.000000 } );

    m_highPassFilter[0]->insertFilterCoef( coef );
    m_highPassFilter[1]->insertFilterCoef( coef );
    m_highPassFilter[2]->insertFilterCoef( coef );
    m_highPassFilter[3]->insertFilterCoef( coef );
}

QString CurrentReaderWorker::createHTTPGetURLFromDataStruct(const ToSendData &data)
{
    QDateTime dateTime;
    dateTime.setTime_t( data.ts/1000 );
    QString url(
                THINGSPEAK_BASE_URL.arg( THINGSPEAK_WRITEAPI_KEY ).arg( data.current[0] ).
                                                                   arg( data.current[1] ).
                                                                   arg( data.current[2] ).
                                                                   arg( data.current[3] ).
                                                                   arg( data.temperature ).
                                                                   arg( data.luminosity ).
                                                                   arg( dateTime.toString( Qt::ISODate ) ) );

    return url;
}


