#ifndef CURRENTREADERWORKER_H
#define CURRENTREADERWORKER_H

/*--- QT Includes. ---*/
#include <QByteArray>
#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QThread>
#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QMap>
#include <QString>
#include <QUrl>
#include <QTimer>

/*--- Wiring Pi Includes. ---*/
#include <wiringPi.h>
#include <mcp3004.h>

/*--- Stl includes. ---*/
#include <memory>
#include <vector>

/*--- User Defined includes. ---*/
#include "RMSCalc.h"

#include "firfilter.h"

#define BASE 100
#define SPI_CHAN 0

struct ToSendData {
    quint64 ts;
    float current[4];
    float temperature;
    float luminosity;
    QNetworkReply *lastReplyObjct;
};

class CurrentReaderWorker : public QObject
{
    Q_OBJECT

public:
    CurrentReaderWorker(const QString & thingSpeakAPIWriteKey, QObject *parent = Q_NULLPTR );
    ~CurrentReaderWorker();

private slots:
    void startWork();

    /**
     * @brief doWork
     * This method must be called periodically. This will read the voltage from the MCP3008
     * (through SPI) and calculate the RMS value from it.
     */
    void doWork();

    /**
     * @brief httpRequestReply
     * This is just an slot meant to be called once that the ThingSpeak server reply.
     * @param reply
     */
    void httpRequestReply(QNetworkReply *reply);

    /**
     * @brief doHttpGETRequest
     * This method must be called periodically (Low frequency). It is meant to get the last calculated
     * RMS current value and create the get request to the Thingspeak. In order to not stop the RMS calculations event
     * loop, this will be connect to an timer running in another QThread using QT::DirectConnection.
     * This will force this method, and the calls inside of it, to the run on a separeted, low priority,
     * event loop.
     */
    void createHttpGETRequest();

    void doHttpGetRequest();

private:
    void startHighPassFilter();

    QString createHTTPGetURLFromDataStruct( const ToSendData & data );

    /*--- Qt Classes and Resources ----*/
    QNetworkAccessManager m_httpRequest;

    QTimer* m_timer;
    QTimer* m_getReqTimer;
    QTimer* m_startAcqTimer;
    QTimer* m_doHTTPReqTimer;

    /*--- Class state variables. ---*/
    /*-- State variables used to keep time. --*/
    u_int64_t m_lastTs, m_currentTs;

    /*-- Last got voltage from MCP3008 and current calculated from it. --*/
    float m_voltage[4], m_current[4];
    float m_temperature, m_luminosity;

    /*-- To send data queue. --*/
    QMap< quint64, ToSendData > m_sendQueue;

    /*-- User defined assistance classes. --*/
    std::vector< std::unique_ptr< RMSCalc > > m_rmsCalc;

    std::vector< std::unique_ptr< FIRFilter<float> > > m_highPassFilter;

    /*--- ... Random Stuff... ---*/
    QFile m_outputFile;
    QString m_outputFileLine;

    /*--- Class constants and operational configurations. ---*/
    static const u_int16_t RMS_CALC_TIMEOUT = 1;                        //Period, in ms, between AD readings and RMS calculations.
    static const u_int16_t HTTP_UPDATE_TIMETOUT = 30000;                 //Period, in ms, between ThingSpeak current updates.
    static const u_int16_t HTTP_DO_GET_TIMETOUT = 20000;                 //Period, in ms, between ThingSpeak current updates.

    static constexpr float AD_REF_VOLTAGE = 5.0f;                       //The voltage used as the reference to the AD converter.
    static constexpr u_int16_t AD_COUNTINGS = 1024;                     //The number of countings of the AD converter. 10 bit converter ( pow(2, 10) = 1024 ).

    const QString THINGSPEAK_WRITEAPI_KEY;                              //Hold the Thingspeak write API Key. "To not hardcore the API Key =P"
    const QString THINGSPEAK_BASE_URL;                                  //Hold the ThingSpeak base URL used to update the current reading.
};

#endif // CURRENTREADERWORKER_H
