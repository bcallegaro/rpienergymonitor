#include "RMSCalc.h"

RMSCalc::RMSCalc(const size_t NmbrReadings)
    : m_readingFifo( new std::deque< float >() )
    , m_periodFifo( new std::deque< float >() )
    , m_sum( 0.0f )
    , MAX_SIZE( NmbrReadings )
{

}

RMSCalc::~RMSCalc()
{
    m_readingFifo->clear();
    m_periodFifo->clear();
}

// warning: unused parameter 'period' [-Wunused-parameter]
float RMSCalc::calculateRMS(float newValue, float period)
{
    float olderValue = 0.0 ;

    newValue = pow( fabs( newValue ), 2 );

    m_readingFifo->push_front( newValue );
    if( m_readingFifo->size() >= MAX_SIZE ) {
        olderValue = m_readingFifo->at( MAX_SIZE-1 );
        m_readingFifo->pop_back();
    }

    m_sum += newValue;
    m_sum -= olderValue;

    return sqrt( m_sum/1000.0f );
}

