QT += core network
QT -= gui

TARGET = RPIHelloWorld
CONFIG += console
CONFIG -= app_bundle

CONFIG += c++11

target.files = RPIHelloWorld
target.path = /home/alarm/

INSTALLS += target

TEMPLATE = app

INCLUDEPATH += /home/bmendonca/dev/SignalProcessors
DEPENDPATH  += /home/bmendonca/dev/SignalProcessors

LIBS += -L/home/bmendonca/dev/SignalProcessors
LIBS += -lSignalProcessors

INCLUDEPATH += /home/bmendonca/dev_tools/rpi-tools/rasp-pi-rootfs/root/usr/local/include
LIBS += -L /home/bmendonca/dev_tools/rpi-tools/rasp-pi-rootfs/root/usr/local/lib

LIBS += -lwiringPi

SOURCES += main.cpp \
    RMSCalc.cpp \
    CurrentReaderWorker.cpp

HEADERS += \
    RMSCalc.h \
    CurrentReaderWorker.h

