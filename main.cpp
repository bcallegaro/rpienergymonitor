
/*--- Qt Includes. ---*/
#include <QCoreApplication>

/*--- User Defined includes. ---*/
#include "CurrentReaderWorker.h"

int main(int argc, char *argv[])
{
    QCoreApplication a( argc, argv );

    CurrentReaderWorker worker( QString( "54KAJP7QP4JQ2V7T" ) );

    return a.exec();
}
